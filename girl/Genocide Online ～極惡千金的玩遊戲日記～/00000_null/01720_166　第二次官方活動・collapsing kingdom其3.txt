作者的話：
手好痛啊〜！

這是跟屁蟲君哦。

========================

「啊啊！瑪利亞大人！！您為何是瑪利亞大人？！」

「……」

穿著軍服的高個子男子，一邊翻動他背上的大大地寫上了“貧乳教主”的斗篷，一邊做出讓人意識到是極度誇張的動作，膝蓋跪於地上、左手放在胸前、右手伸向天空……而用死魚的眼神看著這個情景的，是和他同班的班長蘭花。

「您那嬌小的身驅！！搖曳的低雙馬尾！！」

「……」

甚至連蘭花那樣的死魚般的視線都察覺不到的他……貧乳教主，繼續呼喊著“愛”……因為他也是有青春期特有的那段時期的，每個言行都變得十分誇張亦是沒有辦法的。……是沒有辦法的。

「散佈滿溢可愛的笑容的樣子乃太陽的女神！！那嬌小的身體在到處走動的樣子乃應當保護的幼兒！！」

「……」

停也停不了，他的喊叫停不了……縱使周圍的NPC投來了看見無法理解的事物的視線，縱使蘭花的眼睛變成了更進一步的死魚眼，他亦停不了……不，他是完全沒有停下來的想法。

「從幼女般的身軀傾注出來的那份母性！！」

「……」

「與肉體不相符的那份精神簡直就是聖母！！」

咦？蘭花看起來……？…………這是怎麼一回事呢，蘭花的死魚眼居然浮現出淚水了。……她似乎忍受不了NPC們看向跟這個貧乳教主在一起的她的，那種像是看著同類的視線。

「而且最應該受到愛慕的乃那個——濔迤的平原部分……！！」

「……」

不只是蘭花，連他也流淚了……看起來他是對自己叫喊的內容感到無限感概了。……真是莫名其妙，果真有著如此令人感動的要素在嗎……誰都理解不了。

「啊啊……如此完美的女性，已經別無他者了吧……簡直就是奇蹟！！」

「……」

從貧乳教主這個玩家名稱便可以看得出來，他是重度的貧乳愛好者……不僅如此，對於還患有喜歡年上並且是蘿莉控加母控的性癖雜果賓治的他來說，瑪利亞的出現正正只能說是一個奇蹟……。

「然而！！有光的地方還是會有黑暗！！」

「……」

由潸潸落下淚水的模樣一轉，貧乳教主繃緊了他的表情，迸發出宛如父母的仇人就在眼前般的憤怒的感情。

「儘管時常在瑪利亞大人的身邊侍候著，卻犯下了讓她的容顏變成板著臉的愁容的滔天大罪的惡魔……優！！」

「……」

結果他的叫喊開始連憎惡和嫉妒的感情也混合進去了……他大聲呼喊出對那個，使得令自己墜入情網的女子唯一會露出笑容以外的表情的男人，的負面感情。……蘭花早已變成令外人閉嘴的臉了。

「必定會誅滅那傢伙，救出您的……請稍作等待，瑪利亞大人！！」

「……」

貧乳教主，似乎擅作主張地做好覺悟了……結束最後的喊叫後，他向天空奉上祈禱。……在他背後的蘭花以令外人閉嘴的臉將手放到真理之門上。

「貧乳教義，其一！！——我等不是要被填滿，而是要滿足」

「佛說摩訶般若波羅蜜多心經——」

貧乳教主開始道出自己興辦的宗教——公會的信條了……隨後，不知是不是因此導致超出了羞恥心的容許限度，老家是寺廟的蘭花開始念誦般若心經了。……場面已經一發不可收拾了。

「貧乳教義，其二！！——不要揉，而要捏」

「觀自在菩薩 行深般若波羅蜜多時——」

他，貧乳教主，是停不了的……因為他很年輕嗎，他毫不在意周圍繼續大喊著。……在他身後的蘭花正一心一意、專心致志地……繼續念誦著般若心經。……周圍的NPC們看見瘋子、以及在其身後唸著自己不熟悉的甚麼的少女後，便都快步離開現場了。

「貧乳教義，其三！！——人的應住之處並非山岳，而是平原」

「照見五蘊皆空度一切苦厄——」

到底怎麼了，現在這裡到底怎麼了？！……目擊現場的某個NPC在大喊道，但這種事情是誰都不知道的……就連偶爾目擊到這個情況的運營亦無法掌握究竟發生甚麼了。

「貧乳教義，其四！！——要親手注滿夢想和希望，培養愛」

「舍利子 色不異空 空不異色——」

貧乳教主在喊叫教義的同時，做出宛如寶塚歌劇團般的演戲的動作，而蘭花則在念誦般若心經的同時，用薙刀的金屬箍敲打石板地……這是甚麼儀式嗎？

「貧乳教義，其五！！——奔跑吧！在那平原之上！！」

「色即是空 空即是色 受想行識亦復如是——」

停不了，停不了……根本停不了。貧乳教主發揮出聲樂部的本領還是甚麼的，他開始施展著男高音的顫音，而蘭花則像是為了要完全消除前者的聲音般，激烈且強而有力地用薙刀的金屬箍叩打石板地。

「貧乳教義，其六！！——不是要溫柔地包裹，而是要溫柔地撫摸」

「舍利子 是諸法空相——」

誰、誰去叫衛兵來啊！某NPC如此喊道，但這是毫無意義的……因為就連衛兵都不想靠近過去，所以這是沒有意義的……即使是對熟練的衛兵來說，這樣的事態還是第一次遇見的……沒有辦法。

「貧乳教義，其七！！——惡其脂肪，不惡其巨乳」

「不生不滅 不垢不淨 不增不減 是故空中——」

不顧周圍的變態、和一心一意念誦般若心經的少女的兩人，能向他們說的話甚麼的……對人類來說是已經想不出來的了。……即使如此，如果僅僅有一件事要說的話——

「媽媽—！那些人怎麼了—？」

「噓！不許看！」

——那就是這個場所正被毫無道理的混沌籠罩著。

▼▼▼▼▼▼▼


作者的話：
啊啊，已經變得亂七八糟了啦……。

也就是說，複製貼上初中生是貧乳教主！
猜中的殘機零 先生，恭喜你！！



熟人：甚麼是令外人閉嘴的臉？
作者：就是外人給我閉嘴……。