警察局前擠滿了報道陣容，看來怎麼也不可能和接下來會被釋放出獄的人物接觸了。

一邊遠遠的看著報道陣容一邊考慮該怎麼辦。照這情況來看，她家當然也會有報道陣容吧。因為走投無路的狀況而嘆氣了。

人偶師，朝霧恭子被逮捕是兩周前。她被認為有製造了加害人類的自動人偶的嫌疑。
當然那個人偶就是指７號，那個懷疑是完全的冤枉。
我持續向警察和媒體申訴。７號什麼壊事都沒做。
７號對人的暴力行為是為了幫助我才進行的。

媒體比警察更熱衷於我的話。
７號的行動被作為為了幫助受欺凌的『少女Ａ』的美談報導了。

運氣好輿論站在我這邊，另外在這個事件中沒有出現任何受傷的人，所以朝霧恭子因為證據不充分被施放了。
朝霧恭子被施放是真的僥倖。

可是對我，神尾市子來說，事件並不是這樣就結束了。
倒不如說接下來才是正戲。

雖然想過這樣的話就混進記者裡，衝到從警察局出來的朝霧恭子身邊，但我清楚那樣做事情會變得更麻煩，所以馬上就收回了想法。
因為記者中或許也有知道被作為少女Ａ報導的我的臉的人。結果，直到媒體從朝霧恭子周圍消失為止我都無能為力。

真令人著急。明明不是做這種事的時候。
想這想那之間，看到報道陣容開始行動了。大概是朝霧恭子從警察局出來了吧。我放棄了，向反方向走起來。

明天。即使明天實現不了，近期之內也一定要和朝霧恭子接觸。
首先試著傍晚左右給她家打念信吧。雖然這麼說，但不知道剛剛被釋放大概正忙得不可開交的她會不會響應念信。

＊＊＊＊＊